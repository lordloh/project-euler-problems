#include <iostream>

using namespace std;

#define MAX 1000

int main(){
    unsigned int sum=3, i;
    for(i=5;i<MAX;i++){
         if ((i%3==0) || (i%5==0)){
            sum+=i;
         }
    }
    cout<<sum<<endl;
    return 0;
}
