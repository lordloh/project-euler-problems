#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;
#define MAX 100

unsigned int isPrime(unsigned int num){
    unsigned int isPrime=1, i;
    if (num==2 || num==3) return 1;
    if (num%2==0) return 0;
    for (i=3;i<=sqrt(num);i+=2){
        if (num%i==0){
           isPrime=0;
           break;
        }
    }
    return isPrime;
}

int main(){
    unsigned i;
    long double sum=0;
    for (i=2;i<2000000;i++){
        sum+=(float)((isPrime(i)==1)?i:0);
    }
    cout.setf(ios::fixed);
    cout<<setprecision(0)<<sum<<endl;
    return 0;
}

