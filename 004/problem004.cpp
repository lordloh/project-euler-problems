#include <iostream>
#include <cmath>

using namespace std;

inline int isPlaindrome(unsigned int number){
       unsigned int N=0, B;
       B=number;
       while (number>0){
           N=N*10+number%10;
           number/=10;
       }
       if (N==B) return 1;
       else return 0;
}

int main(){
    unsigned int i,j, num, maxPalindrome=0;
    for (i=999;i>99;i--)
        for (j=999;j>99;j--){
            num=i*j;
            if (isPlaindrome(num)){
               //cout<<num<<endl;
               if (num>maxPalindrome){
                  maxPalindrome=num;
               }
            }
        }
    cout<<maxPalindrome<<endl;
    return 0;
}
