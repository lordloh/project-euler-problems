#include <iostream>
#include <cmath>

using namespace std;

#define NUM 600851475143ULL
//#define NUM 13195 
int isPrime(long long number){
    long long n, m;
    if (number==2) return 1;
    if (number%2==0) return 0;
    m=ceil(pow(number,.5));
    m-=((m%2)?0:1);
    for (n=3;n<=m; n+=2){
        if (number%n==0){
           return 0;
        }
    }
    return 1;
}

int main(){
    unsigned long long int n;
    n=ceil(pow(NUM,.5));
    n+=((n%2)?0:1);
    //factor
    for (;n>2;n-=2){
        if (NUM%n==0){
           if (isPrime(n)){
              cout<<n<<endl;
              return 0;
           }
        }
    }    
    return 0;
}

