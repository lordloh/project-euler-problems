#include <iostream>
#include <cmath>

using namespace std;

int main(){
    unsigned long N=21, i;
    unsigned int div;
    while (true){
          div=1;
          for (i=2;i<21;i++){
              if (N%i!=0){
                div=0;
                 break;
              }
          }
          if (div==1){
             cout<<"::"<<N<<endl;
             break;
          }
          N++;
    }
    return 0;
}
