#include <iostream>
#include <cmath>

using namespace std;
#define MAX 100

int main(){
    unsigned long int sumSq, sqSum;
    double t;
    t=(0.5*(pow((double)MAX,2)+MAX));
    sumSq=pow(t,2);
    sqSum=(2*MAX*MAX*MAX+3*MAX*MAX+MAX)/6;
    cout<<sumSq-sqSum;
    return 0;
}

